package com.example.groom_it.service

import com.example.groom_it.model.ChoiceGroup
import com.example.groom_it.model.SessionChoice

val DEFAULT_GROUP = ChoiceGroup(
    "Default group",
    listOf(
        SessionChoice("1", "1", listOf()),
        SessionChoice("2", "3", listOf()),
        SessionChoice("3", "5", listOf()),
        SessionChoice("4", "9", listOf()),
        SessionChoice("5", "13", listOf()),
        SessionChoice("6", "?", listOf()),
    )
)

object SessionGroupService {
    private var choiceGroups = arrayListOf<ChoiceGroup>(DEFAULT_GROUP)

    fun addGroup(choiceGroup: ChoiceGroup){
        choiceGroups.add(choiceGroup)
    }

    fun getChoiceGroups(): List<ChoiceGroup>{
        return choiceGroups
    }
}