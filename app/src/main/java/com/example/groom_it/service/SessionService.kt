package com.example.groom_it.service

import com.example.groom_it.model.Session
import com.example.groom_it.model.SessionChoice
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response

private const val JSON_BIN_SESSION_URL = "https://api.jsonbin.io/v3/b/646aa47f9d312622a362b00b"
private const val JSON_BIN_MEMBERS_URL = "https://api.jsonbin.io/v3/b/646ad86a8e4aa6225ea1906c"
private const val X_MASTER_KEY = "$2b$10\$EVcvOPKArZlTHt9kHQGg.uLuN2w0lOlWda9LmkPAth6BRtEitB6A2"

val mockedMembers = listOf(
    "Tomas",
    "Radek",
    "Petr",
    "Jana",
    "Ivan"
)

private const val HTTP_OK = 200

object SessionService {
    private val client = OkHttpClient()
    private var sessionMock: Session? = null
    private var currentSessionId: String? = null

    fun getSession(id: String): Response? {
        val builder: Request.Builder = Request.Builder()
        builder.url("$JSON_BIN_SESSION_URL?id=$id")
        val request: Request = builder
            .addHeader("X-MASTER-KEY", X_MASTER_KEY)
            .build()
        try {
            return client.newCall(request).execute()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null;
    }

    fun setCurrentSessionId(value: String?){
        currentSessionId = value
    }

    fun getCurrentSessionId(): String?{
        return currentSessionId;
    }

    // mocked
    fun getSessionMock(id: String?): Session {
        if (sessionMock == null){
            sessionMock = generateSession()
        }
        generateRandomVote()
        return sessionMock as Session
    }

    // API jsonbin do not support PUT so we use mock variable
    fun putSession(session: Session): Session {
        sessionMock = session
        return session
    }

    // API jsonbin do not support POST so we use mock variable
    fun postSessoin(session: Session): Session {
        val newSession = generateSession()
        newSession.name = session.name
        newSession.timeLimit = session.timeLimit
        sessionMock = newSession
        return newSession
    }

    // mocked
    fun getSessionMembers(id: String?): Response?{
        val builder: Request.Builder = Request.Builder()
        builder.url("$JSON_BIN_MEMBERS_URL?id=$id")
        val request: Request = builder
            .addHeader("X-MASTER-KEY", X_MASTER_KEY)
            .build()
        try {
            return client.newCall(request).execute()
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return null;
    }

    private fun generateRandomVote(){
        mockedMembers.forEach {
            val i = (0..5).random()
            (sessionMock as Session).choices[i].vote += it
        }
    }

    private fun generateSession(): Session {
        return Session(
            "1",
            "generated Session",
         5,
            listOf(
                SessionChoice("1", "1", listOf()),
                SessionChoice("2", "3", listOf()),
                SessionChoice("3", "5", listOf()),
                SessionChoice("4", "9", listOf()),
                SessionChoice("5", "13", listOf()),
                SessionChoice("6", "?", listOf()),
            )
        )
    }
}
