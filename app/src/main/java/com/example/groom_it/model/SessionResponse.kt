package com.example.groom_it.model

data class SessionResponse (
        val record: Session,
        val metadata: SessionMetadata
        )