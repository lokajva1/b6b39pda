package com.example.groom_it.model

enum class UserRole {
    USER, CREATOR
}