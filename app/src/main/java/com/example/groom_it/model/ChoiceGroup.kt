package com.example.groom_it.model

data class ChoiceGroup (
    var name: String,
    val choices: List<SessionChoice>
)