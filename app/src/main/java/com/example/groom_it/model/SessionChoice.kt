package com.example.groom_it.model

data class SessionChoice (
    val id: String,
    val name: String,
    var vote: List<String>
        )