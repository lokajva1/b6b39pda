package com.example.groom_it.model

data class MembersResponse (
    val record: List<String>,
    val metadata: SessionMetadata
)