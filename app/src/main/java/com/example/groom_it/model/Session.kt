package com.example.groom_it.model

data class Session (
        val id: String?,
        var name: String,
        var timeLimit: Int,
        val choices: List<SessionChoice>
        )