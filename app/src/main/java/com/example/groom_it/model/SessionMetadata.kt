package com.example.groom_it.model

import java.util.Date

data class SessionMetadata(
    val id: String,
    val private: Boolean,
    val createdAt: Date
)