package com.example.groom_it.activities.running_grooming

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.example.groom_it.R
import com.example.groom_it.activities.vote_result.VoteResultActivity
import com.example.groom_it.common.MenuActivity
import com.example.groom_it.model.Session
import com.example.groom_it.model.UserRole
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

enum class SessionStates {NEW, RUNNING}

class RunningGrooming : MenuActivity() {
    private lateinit var session: Session
    private lateinit var members: List<String>
    private lateinit var optionsChipsGroup: ChipGroup
    private lateinit var membersChipsGroup: ChipGroup
    private var sessionState: SessionStates = SessionStates.NEW

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_running_grooming)

        optionsChipsGroup = findViewById(R.id.optionsChipsGroup)
        membersChipsGroup = findViewById(R.id.membersChipsGroup)

        val bundle = intent.extras
        val gson = Gson()

        val jsonSession = bundle!!.getString("session")
        session = gson.fromJson(jsonSession, Session::class.java)
        val jsonMembers = bundle.getString("members")
        val itemType = object : TypeToken<List<String>>() {}.type
        members = gson.fromJson<List<String>>(jsonMembers, itemType)

        val nameTextView: TextView = findViewById<TextView>(R.id.titleRunningGrooming)
        nameTextView.text = session.name

        val clockTextView: TextView = findViewById<TextView>(R.id.clockCounterRunningGrooming)
        clockTextView.text = session.timeLimit.toString() + "s"

        generateChoices()
        generateMembers()
    }

    fun doGrooming(v: View?){
        if (sessionState == SessionStates.NEW){
            val controlButton = findViewById<Button>(R.id.controlButton)
            controlButton.text = "stop grooming"
            initCountDownTimer()
            sessionState = SessionStates.RUNNING
        } else {
            gotoVoteResult(v)
        }
    }

    private fun initCountDownTimer() {
        object : CountDownTimer(1000L * session.timeLimit, 1000) {
            val textView = findViewById<TextView>(R.id.clockCounterRunningGrooming)

            @SuppressLint("SetTextI18n")
            override fun onTick(millisUntilFinished: Long) {
                textView.text = (millisUntilFinished / 1000).toString() + "s"
            }

            @SuppressLint("SetTextI18n")
            override fun onFinish() {
                gotoVoteResult(null)
            }
        }.start()
    }

    private fun generateChoices(){
        session.choices.forEach {
            val chip = Chip(this)
            chip.text = it.name
            optionsChipsGroup.addView(chip)
        }
    }

    private fun generateMembers(){
        members.forEach {
            val chip = Chip(this)
            chip.text = it
            membersChipsGroup.addView(chip)
        }
    }

    private fun gotoVoteResult(v: View?) {
        val intent = Intent(this, VoteResultActivity::class.java)
        intent.putExtra("role", UserRole.CREATOR.toString())
        startActivity(intent)
    }
}