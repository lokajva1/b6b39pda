package com.example.groom_it.activities.main.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import com.example.groom_it.R
import com.example.groom_it.activities.group_creation.GroupCreationActivity
import com.example.groom_it.activities.running_grooming.RunningGrooming
import com.example.groom_it.databinding.FragmentCreategroomingBinding
import com.example.groom_it.model.MembersResponse
import com.example.groom_it.model.Session
import com.example.groom_it.service.SessionGroupService
import com.example.groom_it.service.SessionService
import com.google.gson.Gson
import java.net.HttpURLConnection

class CreateGroomingFragment : Fragment() {
    private var _binding: FragmentCreategroomingBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentCreategroomingBinding.inflate(inflater, container, false)
        val view = binding.root

        _binding!!.createSession.setOnClickListener {
            val thread = Thread {
                val name = _binding!!.groomingNameInput.text.toString()
                val timeLimit = _binding!!.timeLimitInput.text.toString().toInt()

                val session = Session(null, name, timeLimit, listOf())
                val newSession = SessionService.postSessoin(session)
                SessionService.setCurrentSessionId(newSession.id)
                val gson = Gson()

                val response = SessionService.getSessionMembers(session.id)
                if (response?.code() == HttpURLConnection.HTTP_OK){
                    val membersResponse = gson.fromJson(response.body()?.string(), MembersResponse::class.java)
                    val members = membersResponse.record
                    val intent = Intent (activity, RunningGrooming::class.java)
                    intent.putExtra("session", gson.toJson(newSession))
                    intent.putExtra("members", gson.toJson(members))
                    activity?.startActivity(intent)
                } else {
                    // alert - session id not found
                }
            }
            thread.start()
        }

        _binding!!.newGroup.setOnClickListener {
            val intent = Intent (activity, GroupCreationActivity::class.java)
            activity?.startActivity(intent)
        }

        val selectGroup = _binding!!.selectGroup
        val groupNames = SessionGroupService.getChoiceGroups().map { it.name }.toList()
        val adapterItems = ArrayAdapter<String>(view.context, R.layout.list_item, groupNames)
        selectGroup.setAdapter(adapterItems)
        return view
    }
}