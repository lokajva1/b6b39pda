package com.example.groom_it.activities.main

import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.groom_it.R
import com.example.groom_it.databinding.ActivityMainBinding
import com.example.groom_it.activities.main.adapter.ViewPagerAdapter
import com.google.android.material.tabs.TabLayoutMediator


val groomingArray = arrayOf(
    "Join Grooming",
    "Create Grooming"
)

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val viewPager = binding.viewPager
        val tabLayout = binding.tabLayout

        val adapter = ViewPagerAdapter(supportFragmentManager, lifecycle)
        viewPager.adapter = adapter

        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.text = groomingArray[position]
        }.attach()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        try {
            val intent = Intent()
            intent.action = MediaStore.ACTION_IMAGE_CAPTURE;
            startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return super.onOptionsItemSelected(item)
    }
}