package com.example.groom_it.activities.vote

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.groom_it.R
import com.example.groom_it.activities.vote_result.CHANNEL_ID
import com.example.groom_it.activities.vote_result.CHANNEL_NAME
import com.example.groom_it.activities.vote_result.NOTIF_ID
import com.example.groom_it.common.MenuActivity
import com.example.groom_it.model.Session
import com.example.groom_it.service.SessionService
import com.example.groom_it.activities.vote_result.VoteResultActivity
import com.example.groom_it.databinding.ActivityVoteBinding
import com.example.groom_it.databinding.ActivityVoteResultBinding
import com.example.groom_it.model.UserRole
import com.google.gson.Gson


class VoteActivity : MenuActivity() {
    private lateinit var userName: String
    private lateinit var session: Session
    private lateinit var binding: ActivityVoteBinding

    @SuppressLint("MissingInflatedId", "SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vote)

        val loadingPanel = findViewById<RelativeLayout>(R.id.loadingPanel);
        loadingPanel.visibility = View.GONE;

        val gson = Gson()
        val bundle = intent.extras
        val jsonResponse = bundle!!.getString("session")
        session = gson.fromJson(jsonResponse, Session::class.java)
        userName = bundle.getString("userName").toString()

        val textView: TextView = findViewById<TextView>(R.id.title)
        textView.text = session.name
        initCountDownTimer(session.timeLimit)

        binding = ActivityVoteBinding.inflate(layoutInflater)
    }

    private fun initCountDownTimer(timeLimit: Int) {
        object : CountDownTimer(1000L * timeLimit, 1000) {
            val textView: TextView = findViewById<TextView>(R.id.clock_counter)

            @SuppressLint("SetTextI18n")
            override fun onTick(millisUntilFinished: Long) {
                textView.text = (millisUntilFinished / 1000).toString() + "s"
            }

            @SuppressLint("SetTextI18n")
            override fun onFinish() {
                gotoVoteResult(null)
            }
        }.start()
    }

    fun doVote(v: View?) {
        val loadingPanel = findViewById<RelativeLayout>(R.id.loadingPanel);

        if (loadingPanel.visibility == View.VISIBLE){
            return
        }

        val thread = Thread {
            session.choices[getChoiceId(v)].vote += userName
            SessionService.putSession(session)
        }
        thread.start()
        loadingPanel.visibility = View.VISIBLE;
    }

    private fun getChoiceId(v: View?): Int {
        when (v?.id) {
            R.id.btn1 -> {return 0}
            R.id.btn3 -> {return 1}
            R.id.btn5 -> {return 2}
            R.id.btn9 -> {return 3}
            R.id.btn13 -> {return 4}
            R.id.btnUnknown-> {return 5}
        }
        return 0
    }

    private fun gotoVoteResult(v: View?) {
        createNotification()

        val intent = Intent(this,VoteResultActivity::class.java)
        intent.putExtra("role", UserRole.USER.toString())
        startActivity(intent)
    }

    @SuppressLint("MissingPermission")
    private fun createNotification(){
        createNotifChannel()

        val intent = Intent(this,VoteResultActivity::class.java)
        val pendingIntent = TaskStackBuilder.create(this).run {
            addNextIntentWithParentStack(intent)
            getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        }

        val notif = NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle("Grooming done")
            .setContentText("Grooming " + session.id + " successfully done")
            .setSmallIcon(R.drawable.ic_baseline_info_24)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setContentIntent(pendingIntent)
            .build()

        val notifManger = NotificationManagerCompat.from(this)
        notifManger.notify(NOTIF_ID,notif)
    }

    private fun createNotifChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT).apply {
                lightColor = Color.BLUE
                enableLights(true)
            }
            val manager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(channel)
        }
    }
}