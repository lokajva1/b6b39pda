package com.example.groom_it.activities.vote_result

import android.Manifest
import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.groom_it.R
import com.example.groom_it.activities.main.MainActivity
import com.example.groom_it.common.MenuActivity
import com.example.groom_it.databinding.ActivityVoteResultBinding
import com.example.groom_it.model.Session
import com.example.groom_it.model.UserRole
import com.example.groom_it.notification.NotificationView
import com.example.groom_it.service.SessionService

private val progressBarList = listOf(
    listOf(R.id.vote1ProgressBar, R.id.vote1Text),
    listOf(R.id.vote3ProgressBar, R.id.vote3Text),
    listOf(R.id.vote5ProgressBar, R.id.vote5Text),
    listOf(R.id.vote9ProgressBar, R.id.vote9Text),
    listOf(R.id.vote13ProgressBar, R.id.vote13Text),
    listOf(R.id.voteUnknownProgressBar, R.id.voteUnknownText)
)

const val CHANNEL_ID = "channelID"
const val CHANNEL_NAME = "channelName"
val NOTIF_ID = 0

class VoteResultActivity : MenuActivity() {
    private lateinit var session: Session
    private var totalNumberOfVotes: Int = 0

    @SuppressLint("MissingInflatedId", "MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vote_result)

        session = SessionService.getSessionMock(SessionService.getCurrentSessionId())

        totalNumberOfVotes = session.choices.map { it.vote }
            .flatten()
            .size

        progressBarList.forEachIndexed { idx, it ->
            printVoteResult(it[0], it[1], idx)
        }

        val bundle = intent.extras
        val role = bundle!!.getString("role")
        if (UserRole.USER == UserRole.valueOf(role.toString())) {
            val newGroomingLayout = findViewById<LinearLayout>(R.id.newGroomingButtonLayout)
            newGroomingLayout.visibility = View.GONE
        }
    }

    private fun printVoteResult(progressBarId: Int, textViewId: Int, idx: Int) {
        val voteResult = findViewById<ProgressBar>(progressBarId)
        val voteList = findViewById<TextView>(textViewId)

        voteResult.progress =
            (session.choices[idx].vote.size.toFloat() / totalNumberOfVotes.toFloat() * 100).toInt()
        voteList.text = session.choices[idx].vote.joinToString(", ")
    }

    fun gotoMain(v: View?) {
        val tutorialPage = Intent(this, MainActivity::class.java)
        startActivity(tutorialPage)
    }
}