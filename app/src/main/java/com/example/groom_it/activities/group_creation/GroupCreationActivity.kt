package com.example.groom_it.activities.group_creation

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import androidx.core.view.children
import com.example.groom_it.R
import com.example.groom_it.activities.main.MainActivity
import com.example.groom_it.common.MenuActivity
import com.example.groom_it.model.ChoiceGroup
import com.example.groom_it.model.SessionChoice
import com.example.groom_it.service.SessionGroupService

class GroupCreationActivity : MenuActivity() {
    private lateinit var choiceGroup: LinearLayout
    private var lastIdx = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_creation)
        choiceGroup = findViewById(R.id.choiceGroup)
    }

    fun addInput(v: View){
        var textEdit = EditText(this)
        choiceGroup.addView(textEdit)
        lastIdx++
    }

    fun removeInput(v: View){
        if (lastIdx == 0){
            return
        }
        lastIdx--
        choiceGroup.removeViewAt(lastIdx)
    }

    fun gotoMain(v: View?) {
        val editTextName = findViewById<EditText>(R.id.groupName)
        var values = choiceGroup.children.mapIndexed { idx, value ->
            SessionChoice(idx.toString(),(value as EditText).text.toString(), listOf())
        }
        var choiceGroup = ChoiceGroup(editTextName.text.toString(), values.toList())
        SessionGroupService.addGroup(choiceGroup)
        val tutorialPage = Intent(this, MainActivity::class.java)
        startActivity(tutorialPage)
    }
}