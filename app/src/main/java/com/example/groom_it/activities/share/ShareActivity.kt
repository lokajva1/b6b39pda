package com.example.groom_it.activities.share

import android.os.Bundle
import com.example.groom_it.R
import com.example.groom_it.common.MenuActivity

class ShareActivity : MenuActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share)
    }
}