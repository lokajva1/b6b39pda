package com.example.groom_it.activities.welcome

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.groom_it.R
import com.example.groom_it.activities.main.MainActivity

class WelcomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)
        supportActionBar?.hide()
    }

    fun gotoMain(v: View?) {
        val tutorialPage = Intent(this, MainActivity::class.java)
        startActivity(tutorialPage)
    }
}