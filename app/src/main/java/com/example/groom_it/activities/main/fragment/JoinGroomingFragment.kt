package com.example.groom_it.activities.main.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.groom_it.databinding.FragmentJoingroomingBinding
import com.example.groom_it.model.SessionResponse
import com.example.groom_it.service.SessionService
import com.example.groom_it.activities.vote.VoteActivity
import com.google.gson.Gson
import java.net.HttpURLConnection.HTTP_OK


class JoinGroomingFragment : Fragment() {
    private var _binding: FragmentJoingroomingBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentJoingroomingBinding.inflate(inflater, container, false)
        val view = binding.root

       _binding!!.connect.setOnClickListener {
           val thread = Thread {
               try {
                   val sessionId = _binding!!.idInput.text.toString()
                   val userName = _binding!!.nameInput.text.toString()

                   val response = SessionService.getSession(sessionId)

                   if (response?.code() == HTTP_OK){
                       val intent = Intent (activity, VoteActivity::class.java)
                       val gson = Gson()
                       val sessionResponse = gson.fromJson(response.body()?.string(), SessionResponse::class.java)
                       val session = sessionResponse.record

                       SessionService.setCurrentSessionId(session.id)

                       intent.putExtra("session", gson.toJson(session));
                       intent.putExtra("userName", userName);
                       activity?.startActivity(intent)
                   } else {
                       // alert - session id not found
                   }
               } catch (e: Exception) {
                   e.printStackTrace()
               }
           }
           thread.start()
        }

        return view
    }
}