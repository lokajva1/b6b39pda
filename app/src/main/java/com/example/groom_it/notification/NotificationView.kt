package com.example.groom_it.notification

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.groom_it.R

class NotificationView : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification_view)
        val textView = findViewById<TextView>(R.id.textView)
        //getting the notification message
        val message = intent.getStringExtra("message")
        textView.text = message
    }
}