package com.example.groom_it.common

import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.groom_it.R
import com.example.groom_it.activities.main.MainActivity
import com.example.groom_it.activities.share.ShareActivity
import com.example.groom_it.service.SessionService

open class MenuActivity: AppCompatActivity() {
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_vote, menu)
        val currentSessionId = SessionService.getCurrentSessionId()
        val itemId = menu.findItem(R.id.sessionId)
        itemId.title = "#$currentSessionId"
        if (currentSessionId == null) {
            itemId.isVisible = false
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId;
        if (id == R.id.home){
            SessionService.setCurrentSessionId(null)
            val tutorialPage = Intent(this, MainActivity::class.java)
            startActivity(tutorialPage)
        } else {
            val tutorialPage = Intent(this, ShareActivity::class.java)
            startActivity(tutorialPage)
        }
        return super.onOptionsItemSelected(item)
    }
}